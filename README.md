# Assignment 8: Touch Grass

Various methods of touching grass, analyzed seriously. 

The program accepts the following options:

  -a           Test all methods.

  -s           Test the sequential method.

  -w           Test the wide method.

  -r           Test the random method.

  -v           Show verbose statistics.

  -h           Print this help information.

  -i iters     Set the number of iterations to run (default: size^2).

  -n size      Set the size of the field of grass (1-1024, default: 10).

  -S seed      Set the random seed for -r (default: 7566707).

## Building

Build the program with one of the following commands:

make

make all

make grass

## Running

Run the program with: 

./grass [-OPTIONS]


## Errors

My program's output doesn't necessarily match the reference binary's output exactly--in terms of the messages, not in terms of program functionality. Otherwise, my program works perfectly--with one exception.

For some reason, my program doesn't recognize the capital 'S' option to set a particular seed. I checked the reference grass.c and the getopt process is the same, so I don't know what the issue is.

kthxbye
