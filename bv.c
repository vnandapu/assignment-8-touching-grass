#include "bv.h"

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

//**************************"Yeet!" said the lord, and it was yote.************************//

#define CONTAINER 64

struct BitVector {
    uint32_t length;
    uint64_t *vector;
    uint32_t writes;
};

BitVector *bv_create(uint32_t size) {
    //size is in bits.
    BitVector *bv = (BitVector *) malloc(sizeof(BitVector));
    if (bv) {
        bv->length = size; //# of BITS in the BV
        bv->writes = 0;
        uint32_t vector_size;
        if (size % CONTAINER == 0) {
            vector_size = size / CONTAINER;
        } else {
            vector_size = (size / CONTAINER) + 1; //calc. array size needed to hold size bits
        }

        bv->vector = (uint64_t *) calloc(vector_size, sizeof(BitVector));
        if (!(bv->vector)) {
            free(bv);
            bv = NULL;
        }
    }
    return bv;
}

void bv_delete(BitVector **bv) {
    if (*bv) {
        if ((*bv)->vector) {
            free((*bv)->vector);
        }
        free(*bv);
        *bv = NULL;
    }
    return;
}

uint32_t bv_length(BitVector *bv) {
    if (bv) {
        return bv->length;
    } else {
        printf("Invalid BitVector. Returning zero.\n");
        return 0;
    }
}

bool bv_set_bit(BitVector *bv, uint32_t i) {
    //using logic from bitvector code from Code Comments, cuz I don't wanna thonk.
    if (bv && bv->vector) {
        if (i > bv->length) { //length is max # of bits in array.
            printf("Index out of range.\n");
            return false;
        }
        bv->vector[i / CONTAINER] |= ((uint64_t) 0x1 << i % CONTAINER);
        bv->writes += 1;
        return true;
    } else {
        printf("Invalid BitVector or 'vector' array.\n");
        return false;
    }
}

bool bv_clr_bit(BitVector *bv, uint32_t i) {
    if (bv && bv->vector) {
        if (i > bv->length) {
            printf("Index out of range.\n");
            return false;
        }
        bv->vector[i / CONTAINER] &= ~((uint64_t) 0x1 << i % CONTAINER); //from bv.c from my asgn7
        bv->writes += 1;
        return true;
    } else {
        printf("Invalid BitVector or 'vector' array.\n");
        return false;
    }
}

bool bv_get_bit(BitVector *bv, uint32_t i, bool *bit) {
    if (bv && bv->vector) {
        if (i > bv->length) {
            printf("Index out of range.\n");
            return false;
        }
        if ((bv->vector[i / CONTAINER] >> i % CONTAINER & (uint64_t) 0x01) == 1) {
            *bit = true; //modify param if get_bit is true.
            return true;
        } else {
            return true; //need to return true, since the result of bv_get_bit only checks
                //whether i is a valid index. The bit itself is returned by 'bit'

        } //from bv.c from asgn7 by Yours Truly, which in turn came from bv.8 in code
        //comments. I'm just a phony imposter :^(

    } else {
        printf("Invalid BV or 'vector' array.\n");
        return false;
    }
}

bool bv_set_64(BitVector *bv, uint32_t i) {
    //set every bit in the 64-bit word that i/CONTAINER gives.
    if (bv && bv->vector) {
        if (i > bv->length) {
            return false;
        }
        bv->vector[i / CONTAINER] = ((uint64_t) 0x0 - (uint64_t) 0x1); //gives FFFF...FFFF
        bv->writes += 1;
        return true;
    } else {
        printf("Invalid BV or vector.\n");
        return false;
    }
}

bool bv_clr_64(BitVector *bv, uint32_t i) {
    //clear every bit in the 64-bit word that i/CONTAINER gives.
    if (bv && bv->vector) {
        if (i > bv->length) {
            printf("Index outta range.\n");
            return false;
        }
        bv->vector[i / CONTAINER] = (uint64_t) 0x0;
        bv->writes += 1;
        return true;
    } else {
        printf("Invalid BV or vector.\n");
        return false;
    }
}

uint32_t bv_writes(BitVector *bv) {
    if (bv) {
        return bv->writes;
    } else {
        printf("Invalid BV. Returning 0.\n");
        return 0;
    }
}

void bv_print(BitVector *bv) {
    //[B]rint the [C]itVe[B]tor
    if (bv) {
        bool bit = false;
        for (uint32_t i = 0; i < bv->length; i += 1) {
            if (bv_get_bit(bv, i, &bit)) {
                if (bit == true) {
                    printf("1 ");
                    bit = false;
                } else {
                    printf("0 ");
                }
            }
        }
    }
}
