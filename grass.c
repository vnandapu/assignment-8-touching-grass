#include "field.h"
#include "bv.h"
#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <unistd.h>

#define OPTIONS "aswrvhi:n:s:"

int print_help() {
    printf("SYNOPSIS\n  Compare different methods of touching grass.\n\nUSAGE\n");
    printf("  ./grass [-aswrvh] [-f size] [-n seed]\n\nOPTIONS\n");
    printf("  -a           Test all methods.\n");
    printf("  -s           Test the sequential method.\n");
    printf("  -w           Test the wide method.\n");
    printf("  -r           Test the random method.\n");
    printf("  -v           Show verbose statistics.\n");
    printf("  -h           Print this help information.\n");
    printf("  -i iters     Set the number of iterations to run (default: size^2).\n");
    printf("  -n size      Set the size of the field of grass (1-1024, default: 10).\n");
    printf("  -S seed      Set the random seed for -r (default: 7566707).\n");
    return 0;
}

int main(int argc, char **argv) {
    int opt = 0;
    uint32_t iterations, field_size = 10, seed = 7566707;
    iterations = field_size * field_size;

    bool seq_test = false, rand_test = false, wide_test = false, verbose = false;
    bool iters_given = false;
    opt = getopt(argc, argv, OPTIONS);

    do {
        if (opt == 0) { //no options given
            return print_help();
        }
        switch (opt) {
        case 'h': return print_help(); break;
        case 'a': seq_test = rand_test = wide_test = true; break;
        case 's': seq_test = true; break;
        case 'w': wide_test = true; break;
        case 'r': rand_test = true; break;
        case 'v': verbose = true; break;
        case 'i':
            iters_given = true;
            iterations = atoi(optarg);
            break;
        case 'n':
            field_size = atoi(optarg);
            if (field_size > 1024) {
                printf("Invalid field size. Valid sizes are [0, 1024]. Using 10 instead.\n");
                field_size = 10;
            }
            if (iters_given == false) {
                iterations = field_size * field_size;
            }
            break;
        case 'S': seed = atoi(optarg); break;
        default: return print_help(); break;
        }
    } while ((opt = getopt(argc, argv, OPTIONS)) != -1); //options parsed.

    if (seq_test == true) {
        Field *f1 = field_create(field_size);
        field_touch_sequential(f1, iterations, seed);
        printf("Sequential:\n");
        if (verbose) {
            printf("  Touched blades:         %u\n", field_count(f1));
            printf("  BitVector writes:       %u\n", field_writes(f1));
            float eff = 100 * ((float) field_count(f1) / (float) field_writes(f1));
            printf("  Efficiency:             %.2f%%\n", eff);
            field_delete(&f1);
        } else {
            field_print(f1);
            field_delete(&f1);
        }
    }
    if (wide_test == true) {
        Field *f2 = field_create(field_size);
        field_touch_wide(f2, iterations, seed);
        printf("Wide:\n");
        if (verbose) {
            printf("  Touched blades:         %u\n", field_count(f2));
            printf("  BitVector writes:       %u\n", field_writes(f2));
            float eff = 100 * ((float) field_count(f2) / (float) field_writes(f2));
            printf("  Efficiency:             %.2f%%\n", eff);
            field_delete(&f2);
        } else {
            field_print(f2);
            field_delete(&f2);
        }
    }
    if (rand_test == true) {
        Field *f3 = field_create(field_size);
        field_touch_random(f3, iterations, seed);
        printf("Random:\n");
        if (verbose) {
            printf("  Touched blades:         %u\n", field_count(f3));
            printf("  BitVector writes:       %u\n", field_writes(f3));
            float eff = 100 * ((float) field_count(f3) / (float) field_writes(f3));
            printf("  Efficiency:             %.2f%%\n", eff);
            field_delete(&f3);
        } else {
            field_print(f3);
            field_delete(&f3);
        }
    }
    if (seq_test == rand_test == wide_test == false) {
        printf("No tests given. Pass -h for correct usage, you dumdum.\n");
    }
    return 0;
}
