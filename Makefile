CC = clang
CFLAGS = -Wall -Wextra -Werror -Wpedantic

all: grass

grass: bv.o field.o
	$(CC) $(CFLAGS) bv.o field.o grass.c -o grass

bv.o: bv.h
	$(CC) $(CFLAGS) bv.c -c

field.o:
	$(CC) $(CFLAGS) field.c -c

clean: 
	rm -r *.o grass

format:
	clang-format -i -style=file *.[ch]

