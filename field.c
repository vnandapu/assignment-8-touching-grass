#include "bv.h"
#include "field.h"

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#define flat     1
#define not_flat 0 //0 represents a standing blade, 1 represents flattened

struct Field {
    uint32_t size;
    BitVector *matrix; //array of BVs to represent field, where matrix elements are grass
        //blades
};

Field *field_create(uint32_t size) {
    //create field, allocate mem, allocate matrix
    //matrix will be length: size*size

    Field *f = (Field *) malloc(sizeof(Field));
    if (f) {
        f->size = size;
        f->matrix = bv_create(size * size);
        //BV of size^2
        if (!(f->matrix)) {
            free(f);
        }
    }
    return f;
}

void field_delete(Field **f) {
    if (*f) {
        if ((*f)->matrix) {
            bv_delete(&((*f)->matrix));
        }
        free(*f);
        *f = NULL;
    }
    return;
}

uint32_t field_size(Field *f) {
    if (f) {
        return f->size;
    } else {
        printf("Invalid field.\n");
        return 0;
    }
}

uint32_t field_area(Field *f) {
    if (f) {
        return (f->size * f->size);
    } else {
        printf("Invalid Field.\n");
        return 0;
    }
}

uint32_t field_count(Field *f) {
    if (f) {
        if (f->matrix) {
            uint32_t result = 0;
            bool bit = false, dummy;
            for (uint32_t i = 0; i < bv_length(f->matrix); i += 1) {
                dummy = bv_get_bit(f->matrix, i, &bit);
                if (bit == flat) {
                    result += 1;
                    bit = false;
                }
            }
            return result;
        }
        printf("Invalid field matrix.\n");
        return 0;
    }
    printf("Invalid field.\n");
    return 0;
}

uint32_t field_writes(Field *f) {
    if (f) {
        if (f->matrix) {
            return bv_writes(f->matrix);
        } else {
            printf("Invalid field matrix.\n");
            return 0;
        }
    }
    printf("Invalid field.\n");
    return 0;
}

void field_touch_sequential(Field *f, uint32_t max_iters, unsigned int seed) {
    //touch grass blades starting from 0 to either the max_iters or to end of field.
    seed += 1;
    seed -= 1; //totally using the parameter in a productive way.
    if (f) {
        if (f->matrix) {
            for (uint32_t i = 0; i < bv_length(f->matrix); i += 1) {
                if (i == max_iters) {
                    break;
                }
                bool dummy;
                dummy = bv_set_bit(f->matrix, i);
            }

        } else {
            printf("Invalid field matrix.\n");
            return;
        }
    } else {
        printf("Invalid field.\n");
        return;
    }
}

void field_touch_wide(Field *f, uint32_t max_iters, unsigned int seed) {
    //touch 64 bits at once, "max_iters" number of times, or until end of field is reached.
    seed += 1;
    seed -= 1; //totally using the parameter in a productive way.
    if (f) {
        if (f->matrix) {
            if (max_iters == 0) {
                return;
            }
            uint32_t end_index = 0;
            if (max_iters * 64 >= bv_length(f->matrix)) { //check for range
                end_index = f->size;
            } else {
                end_index = max_iters;
            }
            for (uint32_t i = 0; i < end_index; i += 1) {
                bool dummy;
                dummy = bv_set_64(f->matrix, i * 64);
            }
            return;

        } else {
            printf("Invalid matrix.\n");
            return;
        }
    } else {
        printf("Invalid field.\n");
        return;
    }
}

void field_touch_random(Field *f, uint32_t max_iters, unsigned int seed) {
    if (f) {
        if (f->matrix) {
            srandom(seed); //set seed.
            uint32_t index_to_flatten;
            for (uint32_t i = 0; i < max_iters; i += 1) {
                index_to_flatten = random() % bv_length(f->matrix);
                bool dummy;
                dummy = bv_set_bit(f->matrix, index_to_flatten);
            }
            return;
        } else {
            printf("Invalid matrix.\n");
            return;
        }
    } else {
        printf("Invalid field.\n");
        return;
    }
}
void field_print(Field *f) {
    if (f) {
        if (f->matrix) {
            for (uint32_t i = 0; i < bv_length(f->matrix); i += 1) {
                //print / for standing, _ for flattened grass.
                bool bit = false, dummy;
                dummy = bv_get_bit(f->matrix, i, &bit);
                if (i > 0 && i % f->size == 0) {
                    printf("\n");
                }
                if (bit == flat) {
                    printf("_");
                } else {
                    printf("/");
                }
            }
            printf("\n");
        }
    }
    return;
}
